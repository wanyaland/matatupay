import { Component, OnInit } from '@angular/core';
import { CardService } from '../card.service';
import { AlertService } from '../alert.service';
import { FormGroup,FormControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';

@Component({
  selector: 'app-card-top-up',
  templateUrl: './card-top-up.component.html',
  styleUrls: ['./card-top-up.component.css']
})

export class CardTopUpComponent implements OnInit {
  topUpForm:FormGroup ;
  loading = false;
  submitted = false;

  constructor(private cardService:CardService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.topUpForm = new FormGroup({
      amount: new FormControl()
    });
  }

  get f(){
    return this.topUpForm.controls;
  }

  onSubmit(){
    this.loading = true;
    this.alertService.clear();
    this.cardService.topUp(this.cardService.card,this.f.amount.value).subscribe(
      res => {
        this.alertService.success('Card Topped up ');
        this.loading = false;
      },
      (err) => {
        this.alertService.error('Card Top Up failed ');
        console.log(err);
        this.loading = false;
      }
    )
  }

}
