import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTopUpComponent } from './card-top-up.component';

describe('CardTopUpComponent', () => {
  let component: CardTopUpComponent;
  let fixture: ComponentFixture<CardTopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
