import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { User } from '../models/User';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  user: User;

  constructor(private authService: AuthenticationService, private router:Router) { 
    console.log(this.authService.currentUserValue);
    this.authService.currentUser.subscribe( x => this.user = x);
  }

  ngOnInit() {
  }

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/login');
    console.log('logged out');
  }



}
