import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, 
         MatIconModule, 
         MatListModule,
         MatFormFieldModule,
         MatInputModule,
         MatCardModule,
         MatGridListModule 
        } from '@angular/material';
import { LoginComponent } from './login/login.component';
import { RouterModule,Routes } from '@angular/router';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CardTopUpComponent } from './card-top-up/card-top-up.component';
import { PosComponent } from './pos/pos.component';
import { TaxiComponent } from './taxi/taxi.component';
import { ReportsComponent } from './reports/reports.component';
import { PosMachineComponent } from './pos-machine/pos-machine.component';
import { CardIssueComponent } from './card-issue/card-issue.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AlertComponent } from './alert/alert.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './authentication.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from './user.service';
import { LogoutComponent } from './logout/logout.component';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { HomeComponent } from './home/home.component';
import { AddTaxiComponent } from './add-taxi/add-taxi.component';
import { CardReaderComponent } from './card-reader/card-reader.component';

const appRoutes: Routes = [
  { path:'', redirectTo: 'login', pathMatch:'full' },
  { path: 'login', component: LoginLayoutComponent , 
    children:[
      {path: '', component: LoginComponent}
    ]
  },
  {
    path: 'main', component: HomeLayoutComponent ,
    children: [
      { path:'',component:HomeComponent },
      { path: 'card-issue', component:CardIssueComponent },
      { path: 'card-top-up', component:CardTopUpComponent },
      { path: 'pos', component:PosComponent },
      { path: 'add-taxi', component:AddTaxiComponent },
      { path: 'view-taxis',component: TaxiComponent },
      { path: 'reports' , component: ReportsComponent },
      { path: 'pos-machine' , component: PosMachineComponent },
      { path:'logout', component:LogoutComponent },
    ]
  }
]
@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    LoginComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    NavigationComponent,
    CardTopUpComponent,
    PosComponent,
    TaxiComponent,
    ReportsComponent,
    PosMachineComponent,
    CardIssueComponent,
    ToolbarComponent,
    AlertComponent,
    LogoutComponent,
    HomeComponent,
    AddTaxiComponent,
    CardReaderComponent,
    
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatGridListModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthenticationService, UserService,
             { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
             { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
            ],
  bootstrap: [AppComponent]
})
export class AppModule { }
