import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { AlertService } from '../alert.service';
import { CardService } from '../card.service';
import { MatFormFieldControl } from '@angular/material';

@Component({
  selector: 'app-card-issue',
  templateUrl: './card-issue.component.html',
  styleUrls: ['./card-issue.component.css']
})
export class CardIssueComponent implements OnInit {
  issueForm:FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private alertService: AlertService,
    private cardService: CardService
  ) { }

  ngOnInit() {
    this.issueForm = new FormGroup({
      contact : new FormGroup({
        name: new FormControl(),
        mobile:new FormControl()
      }),
      amount : new FormControl()
    });

    this.issueForm.valueChanges.subscribe (()=> {
      Object.keys(this.issueForm.value).forEach(
        (k) => {
          this.cardService.card[k] = this.issueForm.value[k]
        })
    })
 
  }

  onSubmit(){
    this.submitted = true;
    this.loading = true;
    this.alertService.clear()
    if (this.issueForm.invalid){
      return ;
    }

    this.cardService.saveCard(this.cardService.card).subscribe(
      (res) => {
        this.alertService.success('Card Issued');
        this.loading = false;
      },
      (err) => {
        console.log(err);
        this.alertService.error('Failed to add card: ');
        this.loading = false;
      }
    )
  }

}
