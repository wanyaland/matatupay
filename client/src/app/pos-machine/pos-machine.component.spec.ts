import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineComponent } from './pos-machine.component';

describe('PosMachineComponent', () => {
  let component: PosMachineComponent;
  let fixture: ComponentFixture<PosMachineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosMachineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
