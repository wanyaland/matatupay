import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from './models/User'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = environment.api_base;

  constructor(private http: HttpClient ) { }

  getAll() {
    return this.http.get<User[]>(this.url+'users');
  }

  register(user: User){
    return this.http.post(this.url+'sign_up', user);
  }

  delete(id: number) {
    return this.http.delete(this.url + '/users/${id}');
  }
}
