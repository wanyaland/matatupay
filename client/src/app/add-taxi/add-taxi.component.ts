import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TaxiService } from '../taxi.service';
import {Observable } from 'rxjs';
import { Taxi } from '../taxi';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-add-taxi',
  templateUrl: './add-taxi.component.html',
  styleUrls: ['./add-taxi.component.css']
})

export class AddTaxiComponent implements OnInit {
  taxiForm: FormGroup
  submitted = false;
  loading = false;

  constructor(private taxiService: TaxiService, private alertService: AlertService) { }

  ngOnInit() {

    this.taxiForm = new FormGroup({
      registration: new FormControl(),
      driver: new FormGroup({
        driver_firstname: new FormControl(),
        driver_lastname: new FormControl()
      }),
      owner: new FormGroup({
        contact: new FormGroup({
          name: new FormControl(),
          mobile: new FormControl(),
        })
      }),
      conductor: new FormGroup({
        contact: new FormGroup({
          name: new FormControl(),
          mobile: new FormControl(),
        })
      }),
      line: new FormControl(),
    })

    this.taxiForm.valueChanges.subscribe(() => {
      Object.keys(this.taxiForm.value).forEach((k) => {
        this.taxiService.taxi[k] = this.taxiForm.value[k];
      })
    })

  }

  onSubmit():Observable<Taxi> {
    this.submitted = true;
    this.loading = true;
    this.alertService.clear();

    if (this.taxiForm.invalid){
      return;
    }
    this.taxiService.addTaxi(this.taxiService.taxi).subscribe(
      res => {
        this.alertService.success('Taxi created');
        this.loading = false;
      },
      (err) => {
        this.alertService.error('Failed to add Taxi');
        this.loading = false;
        console.log(err);
      }
    )
  }

}
