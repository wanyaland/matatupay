import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Taxi } from './taxi';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaxiService {

  taxi: Taxi = new Taxi()
  taxis: Array<Taxi> = null;

  constructor(private http:HttpClient) { }

  addTaxi(taxi):Observable<Taxi> {
    let url = environment.api_base+ 'taxis/';
    return this.http.post(url,taxi);
  }

  getTaxis() {
    let url = environment.api_base+ 'taxis/';
    this.http.get(url).subscribe(
      (data: Array<Taxi>) => {
        this.taxis = data;
      });
  }
}
