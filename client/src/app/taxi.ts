export interface Contact{
    name?: string;
    mobile?: string;
}

export class Taxi{
    id?: number;
    registration?: string;
    line?: string;
    conductor?: Conductor;
    driver?: Driver;
    owner?: Owner;
}

export class Conductor{
    id?: number;
    contact?: Contact;
}

export class Owner{
    id?: number;
    contact?: Contact;
}

export class Driver{
    id: number;
    driver_firstname?: string;
    driver_lastname?: string;
}