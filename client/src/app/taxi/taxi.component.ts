import { Component, OnInit } from '@angular/core';
import { TaxiService } from '../taxi.service';
import { Taxi } from '../taxi';

@Component({
  selector: 'app-taxi',
  templateUrl: './taxi.component.html',
  styleUrls: ['./taxi.component.css']
})
export class TaxiComponent implements OnInit {

  taxis: Array<Taxi>;

  constructor(private taxiService: TaxiService) { 
  }

  ngOnInit() {
    this.taxiService.getTaxis();
  }

}
