import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import {HttpClient } from '@angular/common/http';
import { Card } from './Card';
import { MatCardActions } from '@angular/material';

@Injectable({
  providedIn: 'root'
})

export class CardService {
  public card:Card = new Card();
  constructor(private http:HttpClient) { }
  
  saveCard(card): Observable<Card>{
    let url = environment.api_base + 'cards/';
    return this.http.post(url,card);
  }

  topUp(card,amount): Observable<Card> {
    let url = environment.api_base + 'cards/' + card.id ;
    card.amount += amount ;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    return this.http.post(url,card);
  }

}
