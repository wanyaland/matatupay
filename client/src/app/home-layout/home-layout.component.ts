import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css']
})
export class HomeLayoutComponent implements OnInit {
  currentUser: User;

  constructor(private authenticateService: AuthenticationService,
    ) { 
      this.currentUser = authenticateService.currentUserValue;
    }

  ngOnInit() {
  }

}
