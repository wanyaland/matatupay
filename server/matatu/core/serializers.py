import logging 

from django.contrib.auth import get_user_model
from rest_framework import serializers
from core.models import Card,Driver,Conductor,Contact,Taxi,Owner
from rest_framework.permissions import IsAuthenticated


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ['id','name','mobile']

class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ['id','driver_firstname','driver_lastname']

class OwnerSerializer(serializers.ModelSerializer):
    contact = ContactSerializer()
    class Meta:
        model = Owner
        fields = ['id','contact']

class ConductorSerializer(serializers.ModelSerializer):
    contact = ContactSerializer()
    class Meta:
        model = Conductor
        fields = ['id','contact']

class CardSerializer(serializers.ModelSerializer):
    contact = ContactSerializer()
    class Meta:
        model = Card
        fields = ['id','contact','amount']
    
    def create(self,validated_data):
        contact = Contact.objects.create(**validated_data['contact'])
        amount = validated_data['amount']
        return Card.objects.create(amount=amount,contact=contact)
    

class TaxiSerializer(serializers.ModelSerializer):
    driver = DriverSerializer()
    conductor = ConductorSerializer()
    owner = OwnerSerializer()
    class Meta:
        model = Taxi 
        fields = ['id','driver','conductor','owner','line','registration']
    
    def create(self,validated_data):
        conductor_contact = validated_data['conductor']['contact']
        conductor = Conductor.objects.create(
            contact=Contact.objects.create(**conductor_contact)
            )
        owner_contact = validated_data['owner']['contact']
        owner = Owner.objects.create(contact=Contact.objects.create(**owner_contact))
        driver = Driver.objects.create(**validated_data['driver'])
        line = validated_data['line']
        registration = validated_data['registration']
        return Taxi.objects.create(conductor=conductor,owner=owner,driver=driver,line=line,registration=registration)

class UserSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    def validate(self,data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError('Passwords do not match')
        return data 
    
    def create(self,validated_data):
        print(validated_data)
        data = {
            key:value for key,value in validated_data.items()
            if key not in ('password1','password2')
        }
        data['password'] = validated_data['password1']
        return self.Meta.model.objects.create_user(**data)

    class Meta:
        model = get_user_model()
        fields = [ 'id','username','password1','password2',
                   'first_name','last_name']
        read_only_fields = ['id']

class TokenSerializer(serializers.Serializer):
    """
     This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)