from core.models import Taxi,Card
from core.serializers import TaxiSerializer,CardSerializer,UserSerializer,TokenSerializer
from django.contrib.auth import get_user_model,login,logout
from django.contrib.auth.forms import AuthenticationForm
from rest_framework.views import  APIView
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from rest_framework import status,permissions,viewsets,generics,views

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER 
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER 



class TaxiViewSet(viewsets.ModelViewSet):
    #permission_classes = (permissions.IsAuthenticated, )
    queryset = Taxi.objects.all()
    serializer_class = TaxiSerializer

class CardViewSet(viewsets.ModelViewSet):
    queryset = Card.objects.all()
    serializer_class = CardSerializer

class SignUpView(generics.CreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

class LogInView(views.APIView):
    def post(self, request):
        form = AuthenticationForm(data= request.data)
        if form.is_valid():
            user = form.get_user()
            login(request, user=form.get_user())
            serializer = TokenSerializer(data={
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                ),
                "user": user
            })
            serializer.is_valid()   
            return Response(serializer.data)
        else:
            return Response(form.errors, status = status.HTTP_401_UNAUTHORIZED)

class LogOutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated, )
    
    def post(self, *args, **kwargs):
        logout(self.request)
        return Response(status=status.HTTP_204_NO_CONTENT)




