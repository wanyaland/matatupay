from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

class Owner(models.Model):
    contact = models.ForeignKey(Contact,on_delete=models.CASCADE)
    def __str__(self):
        return self.contact.name

class Conductor(models.Model):
    contact = models.ForeignKey(Contact,on_delete=models.CASCADE)
    def __str__(self):
        return self.contact.name
    

class Driver(models.Model):
    driver_firstname = models.CharField(max_length=100)
    driver_lastname = models.CharField(max_length=100)

    def __str__(self):
        return '{} {}'.format(self.driver_firstname,self.driver_lastname)


class Taxi(models.Model):
    registration = models.CharField(max_length=100)
    owner = models.ForeignKey(Owner,on_delete=models.CASCADE)
    driver = models.ForeignKey(Driver,on_delete=models.CASCADE)
    conductor = models.ForeignKey(Conductor,on_delete=models.CASCADE)
    line = models.CharField(max_length=100)

    def __str__(self):
        return self.owner.contact.name

class Card(models.Model):
    contact = models.ForeignKey(Contact,on_delete=models.CASCADE)
    amount = models.PositiveIntegerField()

    def __str__(self):
        return self.contact.name

