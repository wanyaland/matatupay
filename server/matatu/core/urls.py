from django.urls import path,include
from core import views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token


router = routers.DefaultRouter()
router.register(r'taxis',views.TaxiViewSet)
router.register(r'cards',views.CardViewSet)
router.register(r'users',views.UserViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('log_in', views.LogInView.as_view(),name='log_in'),
    path('log_out', views.LogOutView.as_view(), name='log_out'),
    path('api-token-auth/', obtain_jwt_token, name='create-token'),
]
