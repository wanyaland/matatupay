from django.contrib import admin
from core.models import Taxi,Conductor,Owner,Contact,Driver



class TaxiAdmin(admin.ModelAdmin):
    pass

class ConductorAdmin(admin.ModelAdmin):
    pass 

class OwnerAdmin(admin.ModelAdmin):
    pass 

class ContactAdmin(admin.ModelAdmin):
    pass

class DriverAdmin(admin.ModelAdmin):
    pass 

#register models 

admin.site.register(Taxi,TaxiAdmin)
admin.site.register(Conductor,ConductorAdmin)
admin.site.register(Driver,DriverAdmin)
admin.site.register(Owner,OwnerAdmin)
admin.site.register(Contact,ContactAdmin)



