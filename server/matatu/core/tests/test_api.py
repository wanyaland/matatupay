import json 

from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient, APITestCase
from core.models import Contact,Driver,Owner,Conductor,Taxi


PASSWORD = 'pAssw0rd!'



def create_user(username='user@example.com', password=PASSWORD): 
    return get_user_model().objects.create_user(
        username=username, password=password)

def create_contact(name,mobile):
    return Contact.objects.create(name=name,mobile=mobile)

def create_driver(first_name,last_name):
    return Driver.objects.create(driver_firstname=first_name,driver_lastname=last_name)

def create_owner(contact):
    return Owner.objects.create(contact=contact)

def create_conductor(contact):
    return Conductor.objects.create(contact=contact)

def create_taxi(registration,conductor,owner,driver,line):
    return Taxi.objects.create(registration=registration,
                               conductor = conductor,
                               owner = owner,
                               driver = driver,
                               line = line,
    )



class AuthenticationTest(APITestCase):

    def setUp(self):
        self.client = APIClient()
        contact = create_contact(
            name='harold',
            mobile = '0788999982'
        )
        driver = create_driver(
            first_name='harold',
            last_name = 'wanyama'
        )
        owner = create_owner(
            contact = contact
        )
        conductor = create_conductor(
            contact = contact
        )
        self.taxi = create_taxi(
            registration='ugx9891',
            conductor = conductor,
            driver = driver,
            owner = owner,
            line = 'Nansana'
        )
    def login_client(self,username,password):
        # get a token from DRF
        response = self.client.post(
            reverse('create-token'),
            data=json.dumps(
                {
                    'username': username,
                    'password': password
                }
            ),
            content_type='application/json'
        )
        self.token = response.data['token']
        # set the token in the header
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + self.token
        )
        self.client.login(username=username, password=password)
        return self.token

    def test_user_can_sign_up(self):
        response = self.client.post('/api/users/', data={
            'username': 'user@example.com',
            'first_name': 'Test',
            'last_name': 'User',
            'password1': PASSWORD,
            'password2': PASSWORD,
        })
        user = get_user_model().objects.last()
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(response.data['id'], user.id)
        self.assertEqual(response.data['username'], user.username)
        self.assertEqual(response.data['first_name'], user.first_name)
        self.assertEqual(response.data['last_name'], user.last_name)
    
    def test_user_can_log_in(self):
        user create_user()
        response = self.client.post(reverse('log_in'),data = {
            'username' : user.username,
            'password' : PASSWORD,
        })

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIn("token", response.data)
    
    def test_get_all_taxis(self):
        user = create_user()
        self.login_client(username='user@example.com',password=PASSWORD)
        response = self.client.get('/api/taxis/')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        taxis = [str(self.taxi.id)]
        act_taxis =[taxi.get(id) for taxi in response.data]
        self.assertCountEqual(taxis,act_taxis)
    
 